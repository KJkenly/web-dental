import { Component, OnInit } from '@angular/core';
import { PrimeIcons } from 'primeng/api';
import { ListBookingService } from './list-booking.service';
import { LookupService} from '../../../../backend/services/lookup.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-list-booking',
  templateUrl: './list-booking.component.html',
  styleUrls: ['./list-booking.component.scss']
})
export class ListBookingComponent implements OnInit {
//ประกาศตัวเปร
  userId = sessionStorage.getItem('userId');
  events1: any[] = [];
  events: any[] = [];
  transformedData: any[] = [];
  listBooks:any; 
  listHospitalInfo:any = [];
  listService:any = [];
  constructor(
    private ListBookingService: ListBookingService,
    private lookupService:LookupService,
    private router:Router
    ) 
    {
    }
     async ngOnInit() {
     await this.getHospital();
     await this.getService();
     await this.getData();
  }
  async getHospital() {
    const res:any = await this.lookupService.listHospitalInfo();
    let hospital = [];
    hospital = res.data;
    if(hospital.ok){
        this.listHospitalInfo = hospital.results;
    } else {
        alert('error load period')
    }
    console.log("listHospitalInfo",this.listHospitalInfo);

}
  async getService() {
    const res:any = await this.lookupService.listService();
    let service = [];
    service = res.data;
    if(service.ok){
        this.listService = service.results;
    } else {
        alert('error load period')
    }
    // console.log("listService",this.listService);

  }
navigateToOtherPage() {
  this.router.navigate(['/frontend/apps/activity']);
}
thaiDateFormat(date:any){
  const thaiMonth = [
      'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'
  ]
  const newDate = new Date(date);
  let day = newDate.getDate();
  let monthName = thaiMonth[newDate.getMonth()];
  let thaiYear = newDate.getFullYear() + 543;

  let thaidate = day + ' ' + monthName + ' ' + thaiYear;
  return thaidate;
  
}
  async transformedDatalist(listbooks:any){
    if(listbooks.ok){     
      // console.log("listBooks : ",listbooks.results);    
      await listbooks.results.forEach(
        async (item: 
          { 
            service_type: { service_type_name: any; icon_filename:any}[]; 
            slot: { slot_name: any; hospital_id: any; service_id:any; slot_date:string | number | Date}[];
            reserve_date: string | number | Date; 
            is_confirm: boolean;
            customer:{customer_name:any}[];
          }) => 
        {     
          let _is_confirm = item.is_confirm ? 'ยืนยันการจอง' : 'รอยืนยัน';
        this.events1.push({
          service_type_name: item.service_type[0]?.service_type_name || 'no data',  
          icon: "/"+item.service_type[0]?.icon_filename ||'no data', 
          slot_name: item.slot[0]?.slot_name || 'no data',  
          service_id: item.slot[0]?.service_id || 'no data',
          hospital_id: item.slot[0]?.hospital_id || 'no data',  
          customer_name: item.customer[0]?.customer_name || 'no data',
          date: new Date(item.slot[0]?.slot_date).toISOString(),
          is_confirm:_is_confirm
          
        });
        this.events = this.events1;
        for(let a of this.events){             
          let c:any = this.listHospitalInfo.find((c:any) => c.hospital_id === a.hospital_id);
          a.hospital_name = c.hospital_name;
        }
        for(let a of this.events){             
          let c:any = this.listService.find((c:any) => c.service_id = a.service_id);
          a.service_name = c.service_name;
      }
        // console.log("events: ",this.events);
      }
      );      

  } else {
      alert('Error loading clinics');
  }
  }
  async getData() {
    // this.blockedPanel = true;
    try{
        const res:any  = await this.ListBookingService.list(this.userId);
        let listbooks = [];
        listbooks = res.data;  
        this.transformedDatalist(listbooks);     
        
    }catch(error) {
        console.log(error);
    }
  }

}
