import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class ServicesService {
    // pathPrefix: any = `:40014`
    pathPrefixAuth: any = `:40001`;

    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}${this.pathPrefixAuth}`,
    });

    constructor() {
        this.axiosInstance.interceptors.request.use((config) => {
            const token = sessionStorage.getItem('token');
            if (token) {
                config.headers['Authorization'] = `Bearer ${token}`;
            }
            return config;
        });
    }

  
    async list() {
        const url = `/frontend/lookup/getLookupService`;
        return this.axiosInstance.get(url);
    }
    
    async listSlot(id:number) {
        const url = `/frontend/lookup/getLookupDateSlot/`+ id;
        return this.axiosInstance.get(url);
    }

}