# Stage 1
FROM node:20-alpine as builder

LABEL maintainer="Thawatchai Saengduan <thawatchai.sea2@gmail.com>"

WORKDIR /app

RUN apk add --upgrade --no-cache --virtual deps python3 build-base git

COPY . .

RUN npm i
RUN npm run build

# STAGE 2
CMD ["pm2-runtime", "process.json"]
