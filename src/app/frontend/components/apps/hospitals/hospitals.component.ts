import { Component, OnInit } from '@angular/core';
import { Table } from 'primeng/table';
import { Router } from '@angular/router';

import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { HospitalsService } from './hospitals.service';

@Component({
    selector: 'app-backend-hospitals',
    templateUrl: './hospitals.component.html',
    styleUrls: ['./hospitals.component.scss'],
})
export class HospitalsComponent {


    blockedPanel: boolean = false;

    hospitals: any;

    listHospitals: any;
    listSlot: any;
    selectedHospitals: any[]= [];
    filterSlots: any;

    displayForm: boolean = false;

    hospitalCode: string = '';
    hospitalName: string = '';
    hospitalsActive: boolean = true;

    hospitalCodeEdit: string = '';
    hospitalIdEdit: number = 0;
    hospitalNameEdit: string = '';
    hospitalActiveEdit: boolean = true;

    isAdd: boolean = false;
    isEdit: boolean = false;
    checkboxState: boolean[] = [];

    hospitalsStatus = [
        { label: 'Active', value: true },
        { label: 'Inactive', value: false },
    ];

    constructor(
        private layoutService: LayoutService,
        private hospitalsService: HospitalsService,
        private router: Router
    ) {
        this.listSlot = JSON.parse(sessionStorage.getItem('slots') || '[]');
    }

    async ngOnInit() {
        await this._prepareHospital();
        console.log('list hospital:', this.listHospitals);
        // this.selectedHospitals = this.listHospitals;
        // await this.getData();
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }

    _prepareHospital() {
        this.listHospitals = Array.from(
            new Set(this.listSlot.map((event: any) => event.hospital_id))
        ).map((hospital_id: any) => {
            return this.listSlot.find((event: any) => event.hospital_id === hospital_id);
        });
        // console.log('hospital:', this.listHospitals);
    }

    // _prepareData from slot
    private _prepareData(data: any) {
        this.filterSlots = [];
        this.listSlot.map((item: any) => {
            data.includes(item.hospital_id) ? item.checked = true : item.checked = false;
        });
        this.filterSlots = this.listSlot.filter((item: any) => item.checked === true);
    }

    // go to calendar
    calendar() {
        this._prepareData(this.selectedHospitals);
        sessionStorage.setItem('slots', JSON.stringify(this.filterSlots));
        this.router.navigate(['/frontend/apps/calendar']);
    }

    getHospital(e: any,id:number) {
        console.log('e:', e);

        if (e.checked.length > 0) {
            this.selectedHospitals.push(id);
        } else {
            this.selectedHospitals.splice(this.selectedHospitals.indexOf(id), 1);
        }
        console.log('selectedHospitals:', this.selectedHospitals);
    }

    clear() {
        this.selectedHospitals = [];
    }

    activities() {
		this.router.navigate(['/frontend/apps/activities']);
    }
    // get Services data from API
    async getData() {
        this.blockedPanel = true;
        try {
            const res: any = await this.hospitalsService.list();
            let hospitals = [];
            hospitals = res.data;
            console.log('hospitals:', hospitals);
            if (hospitals.ok) {
                this.hospitals = hospitals.results;
            } else {
                alert('Error loading hospitals');
            }
            this.blockedPanel = false;
        } catch (error) {
            this.blockedPanel = false;

            console.log(error);
        }
    }

    // display form add data
    displayFormAdd() {
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;
        this.hospitalCode = '';
        this.hospitalName = '';
    }

    // display form edit data
    displayFormEdit(data: any) {
        this.isAdd = false;
        this.isEdit = true;
        this.hospitalIdEdit = data.hospital_id;
        this.hospitalCodeEdit = data.hospital_code;
        this.hospitalNameEdit = data.hospital_name;
        this.hospitalActiveEdit = data.is_active;
        this.displayForm = true;
    }

    // function save data
    async save() {
        let data = {
            hospital_code: this.hospitalCode,
            hospital_name: this.hospitalName,
        };

        // save data
        await this.hospitalsService.save(data);

        // close form
        this.displayForm = false;

        //refresh data
        await this.getData();
    }

    // function update data
    async update() {
        let id = this.hospitalIdEdit;
        let body = {
            hospital_code: this.hospitalCodeEdit,
            hospital_name: this.hospitalNameEdit,
            is_active: this.hospitalActiveEdit,
        };

        // update data
        await this.hospitalsService.update(id, body);

        // close form
        this.displayForm = false;

        // refresh data
        await this.getData();
    }

    // set is active = false
    async disable(id: number) {
        let body = {
            is_active: false,
        };
        await this.hospitalsService.update(id, body);

        // refresh data
        await this.getData();
    }

    // set is active = true
    async enable(id: number) {
        let body = {
            is_active: true,
        };
        await this.hospitalsService.update(id, body);

        // refresh data
        await this.getData();
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image =
            this.layoutService.config.colorScheme === 'dark'
                ? 'line-effect-dark.svg'
                : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }
}
