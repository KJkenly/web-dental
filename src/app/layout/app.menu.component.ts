import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { LayoutService } from './service/app.layout.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[] = [];
    // set isAdmin (value: boolean)
    isAdminHospital: boolean = false;
    isAdminCenter: boolean = false;
    isUserHospital: boolean = false;

    userRole : any ; 

    constructor(public layoutService: LayoutService) {
    }

    ngOnInit() {
        this.userRole = sessionStorage.getItem('userRole');
        
        if(this.userRole == '2'){
            this.isAdminHospital = true;
        }else if(this.userRole== '1'){
            this.isAdminCenter = true;
        } else if(this.userRole == '3'){
            this.isUserHospital = true;
        }
        this.model = this.createMenu();
    }

    // set menu (value: MenuItem[])
    private createMenu(): MenuItem[] {
        // check is admin center
        if(this.isAdminCenter){
            return [
                { label: 'จัดการผู้ใช้', icon: 'pi pi-users', routerLink: ['/backend/user'] },
                { label: 'จัดการสิทธิ์', icon: 'pi pi-tag', routerLink: ['/backend/role'] },
                { label: 'จัดการหน่วยบริการ', icon: 'pi pi-building', routerLink: ['/backend/hospital'] },
                { label: 'จัดการคลินิก', icon: 'pi pi-box', routerLink: ['/backend/clinic'] },
                { label: 'จัดการบริการ', icon: 'pi pi-circle', routerLink: ['/backend/service'] },
                {
                    label: 'ศูนย์ช่วยเหลือ', icon: 'pi pi-question-circle',            
                    items: [
                        { label: 'คำถามที่พบบ่อย', icon: 'pi pi-comments', routerLink: ['/helper/faq'] },
                        { label: 'คู่มือต่าง ๆ', icon: 'pi pi-book', routerLink: ['/helper/help'] },
                        { label: 'ติดต่อเรา', icon: 'pi pi-phone', routerLink: ['/helper/contact'] }
                    ]
                },
            ];
        } else 
        // check is admin hospital
        if(this.isAdminHospital){
            return [
                { label: 'Home', icon: 'pi pi-home', routerLink: ['/backend'] },

                { label: 'จัดการผู้ใช้', icon: 'pi pi-users', routerLink: ['/backend/user'] },
                { label: 'จัดการ Slot', icon: 'pi pi-calendar', routerLink: ['/backend/slot'] },
                { label: 'จัดการช่วงเวลา', icon: 'pi pi-clock', routerLink: ['/backend/period'] },

                {
                    label: 'ศูนย์ช่วยเหลือ', icon: 'pi pi-question-circle',            
                    items: [
                        { label: 'คำถามที่พบบ่อย', icon: 'pi pi-comments', routerLink: ['/helper/faq'] },
                        { label: 'คู่มือต่าง ๆ', icon: 'pi pi-book', routerLink: ['/helper/help'] },
                        { label: 'ติดต่อเรา', icon: 'pi pi-phone', routerLink: ['/helper/contact'] }
                    ]
                },
            ];
        } else 
        // check is user hospital
        if(this.isUserHospital){
            return [
                { label: 'Home', icon: 'pi pi-home', routerLink: ['/backend'] },
                { label: 'จัดการการจองคิว', icon: 'pi pi-th-large', routerLink: ['/backend/management'] },
                {
                    label: 'ศูนย์ช่วยเหลือ', icon: 'pi pi-question-circle',            
                    items: [
                        { label: 'คำถามที่พบบ่อย', icon: 'pi pi-comments', routerLink: ['/helper/faq'] },
                        { label: 'คู่มือต่าง ๆ', icon: 'pi pi-book', routerLink: ['/helper/help'] },
                        { label: 'ติดต่อเรา', icon: 'pi pi-phone', routerLink: ['/helper/contact'] }
                    ]
                },
            ];
        } else {
            return [
                { label: 'ประวัติการจองคิว', icon: 'pi pi-calendar-minus', routerLink: ['/frontend/apps/list-booking'] },
                {
                    label: 'ศูนย์ช่วยเหลือ', icon: 'pi pi-question-circle',            
                    items: [
                        { label: 'คำถามที่พบบ่อย', icon: 'pi pi-comments', routerLink: ['/helper/faq'] },
                        { label: 'คู่มือต่าง ๆ', icon: 'pi pi-book', routerLink: ['/helper/help'] },
                        { label: 'ติดต่อเรา', icon: 'pi pi-phone', routerLink: ['/helper/contact'] }
                    ]
                },
            ];
        }
    }
}
