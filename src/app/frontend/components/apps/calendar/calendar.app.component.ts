import { Component, OnInit } from '@angular/core';
import { SlotService } from './calendar.service';
import { DateTime } from 'luxon';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

// @fullcalendar plugins
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import { CalendarOptions } from '@fullcalendar/core';
import { LookupService } from '../../../service/lookup.service';
import * as _ from 'lodash';


@Component({
    templateUrl: './calendar.app.component.html',
    styleUrls: ['./calendar.app.component.scss'],
})
export class CalendarAppComponent implements OnInit {

    jwtHelper: JwtHelperService = new JwtHelperService();
    events: any[] = [];

    today: string = '';

    calendarOptions: any;

    showDialog: boolean = false;

    clickedEvent: any = null;

    dateClicked: boolean = false;

    edit: boolean = false;

    tags: any[] = [];

    view: string = '';

    changedEvent: any;
    selectedSlotID: any;

    now = DateTime.now();

    selectServiceTypeId: Number;
    listSlot: any = [];
    listSlotByID: any = [];
    listHospitals: any = [];
    listPeriods: any = [];
    listCustomers: any = [];
    selectHospitals: any = [];
    selectDate: any;
    listAvialableSlot: any = [];
    listAvialableHospital: any = [];

    isLoading: boolean = false;

    customer: any = {};
    customerName: string = '';
    customerTel: string = '';
    customerLineId: string = '';
    customerAge: string = '';
    customerCID: string = '';
    customerPassport: string = '';
    customerChronics: string = '';

    userId: Number;
    serviceTypeId: any;
    selectSlot: any;


    constructor(
        private slotService: SlotService, private lookupService: LookupService, private router: Router
    ) {
        this.selectServiceTypeId = Number(
            sessionStorage.getItem('serviceTypeId')
        );
        this.serviceTypeId = Number(sessionStorage.getItem('serviceTypeId'));
        this.userId = Number(sessionStorage.getItem('userId'));

        this.listSlot = JSON.parse(sessionStorage.getItem('slots') || '[]');
        // console.log('slots:', this.listSlot);

    }

    ngOnInit(): void {
        this.today = this.now.setLocale('th-TH').toISODate();

        this._prepareCalendar();
        this.getCustomer();
        // this.getHospital();
    }


    async getCustomer() {
        const userId: any = sessionStorage.getItem('userId');
        let _profile: any = await this.lookupService.getProfilesById(userId);
        if (_profile.data.results.length > 0) {
            const res: any = await this.lookupService.listCustomerByCID(_profile.data.results[0].cid);
            if (res.data.results.length == 0) {
                let profile = _profile.data.results[0];
                this.customerName = profile.title + ' ' + profile.fullname;
                this.customerTel = profile.phone_number;
                this.customerCID = profile.cid;
                this.listCustomers = {
                    customer_name: this.customerName,
                    phone_number: this.customerTel,
                    cid: this.customerCID,
                }
            } else {
                this.listCustomers = res.data.results[0];
                this.customerName = this.listCustomers.customer_name;
                this.customerTel = this.listCustomers.phone_number;
                this.customerLineId = this.listCustomers.line_id;
                this.customerAge = this.listCustomers.age;
                this.customerCID = this.listCustomers.cid;
                this.customerPassport = this.listCustomers.passport;
                this.customerChronics = this.listCustomers.chronic;
            }
            let customer = [];

            customer = res.data;
            if (customer.ok) {
                this.listCustomers = customer.results[0];
                console.log(this.listCustomers);

                // this.customerCID=this.listCustomers.customer_name;

            } else {
                alert('error load period')
            }
        }
    }

    async getHospital() {
        const res: any = await this.lookupService.listHospital();
        let hospital = [];
        hospital = res.data;
        if (hospital.ok) {
            this.listHospitals = hospital.results;
        } else {
            alert('error load period')
        }
    }

    async getByServiceID(hospital: any) {
        // console.log(hospital);
        // this.selectHospitals=hospital_id.hospital_id;
        // console.log(this.selectHospitals);
        const hospital_id = hospital.hospital_id;
        let _slot = this.listSlot.filter((event: any) => event.hospital_id == hospital_id && DateTime.fromISO(event.slot_date)
            .setLocale('th-TH')
            .toISODate() == this.selectDate);

        for (let v of _slot) {
            v.valuename = v.slot_name + ' วันที่ ' + this.thaiDateFormat(v.slot_date) + ' (' + v.period_name + 'น.)'
            console.log(v);
        }

        this.listSlotByID = _slot;
        // console.log(this.listSlotByID);
        // console.log(this.selectSlot);
    }

    async slotchange(slotData: any) {
        this.selectedSlotID = slotData.slot_id
        // console.log(this.selectSlot);

    }

    thaiDateFormat(date: any) {
        const thaiMonth = [
            'ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'
        ]
        const newDate = new Date(date);
        let day = newDate.getDate();
        let monthName = thaiMonth[newDate.getMonth()];
        let thaiYear = newDate.getFullYear() + 543;

        let thaidate = day + ' ' + monthName + ' ' + thaiYear;
        return thaidate;

    }

    _prepareHospital() {
        this.listHospitals = Array.from(
            new Set(this.listSlot.map((event: any) => event.hospital_id))
        ).map((hospital_id: any) => {
            return this.listSlot.find((event: any) => event.hospital_id === hospital_id);
        });
        // console.log('hospital:', this.listHospitals);
    }

    selectHospital(hospital_id: any) {
        this.listPeriods = this.listSlot.filter(
            (event: any) => event.hospital_id === hospital_id
        );
        // console.log('periods:', this.listPeriods);
    }

    async _prepareCalendar() {
        let events = await this.listSlot.map((event: any) => ({
            title: event.slot_name,
            start: DateTime.fromISO(event.slot_date)
                .setLocale('th-TH')
                .toISODate(),
            display: 'background',
            backgroundColor: '#90F871',
            borderColor: '#90F871',
            textColor: '#010101',
            id: event.slot_id,
        }));

        this.calendarOptions = {
            plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
            height: 320,
            initialView: 'dayGridMonth',
            initialDate: this.today,
            headerToolbar: {
                left: 'prev',
                center: 'title',
                right: 'next',
            },
            locale: 'th-TH',
            editable: true,
            selectable: true,
            selectMirror: true,
            dayMaxEvents: true,
            events: events,
            selectLongPressDelay: 100, //ค่าเร่ิมต้น สำหรับ teblet mobile  = 1000ms  คือต้องกดค้าง 1000 ms
            // eventClick: (e: MouseEvent) => this.onEventClick(e), //เมื่อคลิกเลือก event
            select: (e: MouseEvent) => this.onDateSelect(e), //เมื่อเลือกวันที่
            // dayRender: (arg: any) => this.handleDayRender(arg), // disable day if no event
        };
    }

    handleDayRender(arg: any) {
        // console.log(arg);

        // Check if there is an event for the current date
        const hasEvent = arg.some(
            (event: any) => event.start === arg.date.toISOString().split('T')[0]
        );

        // If no event, disable the day
        if (!hasEvent && arg.el) {
            // console.log(arg);
            arg.el.classList.add('fc-day-disabled');
            // console.log('no event');
        }
    }

    isHoliday(date: Date): boolean {
        // Implement your logic to determine if the date is a holiday
        // Replace this with your own holiday detection algorithm
        // For example, you might have a list of holiday dates to check against
        const holidayDates = ['2024-01-10'];
        return holidayDates.some((holiday) => date.toISOString().split('T')[0] === holiday);
    }


    onEventClick(e: any) {
        this.clickedEvent = e.event;
        // console.log('click:', this.clickedEvent);
        let plainEvent = e.event.toPlainObject({
            collapseExtendedProps: true,
            collapseColor: true,
        });
        this.view = 'display';
        this.showDialog = true;

        this.changedEvent = { ...plainEvent, ...this.clickedEvent };
        this.changedEvent.start = this.clickedEvent.start;
        this.changedEvent.end = this.clickedEvent.end
            ? this.clickedEvent.end
            : this.clickedEvent.start;
    }

    onDateSelect(e: any) {
        this.view = 'new';

        this.selectDate = e.startStr; // set date
        this.listAvialableSlot = []; // clear list slot
        // filter slot by date
        this.listAvialableSlot = this.listSlot.filter((event: any) => DateTime.fromISO(event.slot_date)
            .setLocale('th-TH')
            .toISODate() == e.startStr);
        // map hospital from slot by slot date
        let _hospital = Array.from(
            new Set(this.listAvialableSlot.map((event: any) => event.hospital_id))
        ).map((hospital_id: any) => {
            return this.listAvialableSlot.find((event: any) => event.hospital_id === hospital_id);
        });

        // clear list hospital
        this.listAvialableHospital = [];
        for (let v of _hospital) {
            let data = {
                hospital_id: v.hospital_id,
                hospital_name: v.hospital_name
            }
            this.listAvialableHospital.push(data);
        }

        this.changedEvent = {
            ...e,
            title: null,
            description: null,
            location: null,
            backgroundColor: null,
            borderColor: null,
            textColor: null,
            tag: { color: null, name: null },
        };

        // check if slot is empty
        if (this.listAvialableSlot.length > 0) {
            this.showDialog = true;
        }
    }

    handleSave() {
        if (!this.validate()) {
            alert('กรุณากรอกข้อมูลให้ครบถ้วน');
            return;
        } else {
            this.showDialog = false;
            let data = {
                customer_name: this.customerName,
                phone_number: this.customerTel,
                line_id: this.customerLineId,
                age: this.customerAge,
                cid: this.customerCID,
                passport: this.customerPassport,
                chronic: this.customerChronics,
                customer_status: true,
                register_date: "2567-01-09T01:58:51.000Z",
            };
            // console.log(data);
            this.customer = data;
      
            this.save(this.customer);

            this.clickedEvent = {
                ...this.changedEvent,
                backgroundColor: this.changedEvent.tag.color,
                borderColor: this.changedEvent.tag.color,
                textColor: '#212121',
            };

            if (this.clickedEvent.hasOwnProperty('id')) {
                this.events = this.events.map((i) =>
                    i.id.toString() === this.clickedEvent.id.toString()
                        ? (i = this.clickedEvent)
                        : i
                );
            } else {
                this.events = [
                    ...this.events,
                    {
                        ...this.clickedEvent,
                        id: Math.floor(Math.random() * 10000),
                    },
                ];
            }
            this.calendarOptions = {
                ...this.calendarOptions,
                ...{ events: this.events },
            };
            this.clickedEvent = null;
        }
    }

    onEditClick() {
        this.view = 'edit';
    }

    delete() {
        this.events = this.events.filter(
            (i) => i.id.toString() !== this.clickedEvent.id.toString()
        );
        this.calendarOptions = {
            ...this.calendarOptions,
            ...{ events: this.events },
        };
        this.showDialog = false;
    }

    validate() {
        if(this.selectHospitals && this.selectSlot && this.customerName && this.customerTel && this.customerCID){
            return true;
        } else {
            return false;
        }
    }


    async save(customer: any) {
        const check = await this.lookupService.listCustomerByCID(customer.cid);
        let customerId: number = 0;
        if (check.data.results.length == 0) {

            try {
                const res: any = await this.slotService.saveCustomer(customer);
                let data = res.data;
                // console.log(data);            
                if (data.results.length > 0) {
                    customerId = data.results[0].customer_id;
                    // console.log('saveCustomer:', data.results);
                }
            } catch (error) {
                console.log(error);
            }
        }
        else {
            customerId = check.data.results[0].customer_id;
        }
        this.saveReserves(customerId);
    }

    async saveReserves(customerId: any) {
        let reserve = {
            customer_id: customerId,
            slot_id: this.selectedSlotID,
            user_id: this.userId,
            service_type_id: this.selectServiceTypeId,
            reserve_date: this.today,
        };
        // console.log(reserve);

        try {
            const check : any = await this.lookupService.getReservesByCID(customerId);
            if(check.data.results.length > 0){
                alert('คุณได้ทำการจองคิวไปแล้ว');
                this.router.navigate(['/frontend/apps/list-booking']);
                return;
            } else {
                const res: any = await this.slotService.saveReserves(reserve);
                let data = res.data;
                if (data.results.length > 0) {
                    // console.log('saveReserves:', data.results);
                    this.router.navigate(['/frontend/apps/list-booking']);
                }    
            }

        } catch (error) {
            console.log(error);
        }
    }

    displayClear(){
        this.customerCID = '';
        this.customerName = '';
        this.customerTel = '';
        this.customerLineId = '';
        this.customerAge = '';
        this.customerChronics = '';
        this.selectHospitals = null;
        this.selectSlot = null;
    }
}
