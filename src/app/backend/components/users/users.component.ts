import { Component, OnInit } from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { UsersService } from './users.service';
import { Table } from 'primeng/table';

@Component({
    selector: 'app-backend-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
    blockedPanel: boolean = false;
    // profiles
    user_id: any;
    cid: any;
    fullname: any;
    phone_number: any;
    position: any;
    title: any;
    is_hospital: boolean = true;
    is_service: boolean = false;
    // list hospital
    hospitals: any;

    // edit profiles
    cidEdit: any;
    fullNameEdit: any;
    hospitalId: any;
    phoneNumberEdit: any;
    posiTionEdit: any;
    tiTleEdit: any;

    // service_id
    selectService: any;
    services: any;

    serviceall: any;

    serviceId: any;

    hospitalBack: any;

    // edit Roles
    selectedDrop: any;
    // roles
    roles: any;
    profiles: any;
    rolesall: any;

    users: any;
    displayForm: boolean = false;

    userName: string = '';
    passWord: string = '';
    usersActive: boolean = true;

    userIdEdit: number = 0;
    userNameEdit: string = '';
    passWordEdit: string = '';
    passWordD: string = '';
    userActiveEdit: boolean = true;

    isAdd: boolean = false;
    isEdit: boolean = false;

    messages: any | undefined;

    usersStatus = [
        { label: 'Active', value: true },
        { label: 'Inactive', value: false },
    ];

    constructor(
        private layoutService: LayoutService,
        private usersService: UsersService
    ) {}

    async ngOnInit() {
        await this.getHospitalData();
        await this.getRolesAllData();
        await this.getServiceData();
        await this.getData();
    }

    onGlobalFilter(table: Table, event: Event) {
        table.filterGlobal(
            (event.target as HTMLInputElement).value,
            'contains'
        );
    }

    // get profiles
    async getProfilesData(id: any) {
        const res: any = await this.usersService.getProfilesById(id);
        let profiles = [];
        profiles = res.data;
        // console.log('profiles:', profiles);
        if (profiles.ok) {
            this.profiles = profiles.results[0];
        } else {
            alert('Error loading users');
        }
    }

    // get hospital
    async getHospitalData() {
        let res: any;
        let id: any = sessionStorage.getItem('hospitalId');

        if (id == 1) {
            res = await this.usersService.listHospital();
        } else {
            res = await this.usersService.listHospitalId(id);
        }

        let hospital = [];
        hospital = res.data;
        if (hospital.ok) {
            this.hospitals = hospital.results;
            this.hospitalBack = hospital.results;
        } else {
            alert('Error loading users');
        }
    }

    // get Roles
    async getRolesData(id: any) {
        const res: any = await this.usersService.getRolesById(id);
        this.roles = res.data.results;
    }

    // get RolesAll
    async getRolesAllData() {
        const res: any = await this.usersService.listRoles();
        this.rolesall = [];

        if (sessionStorage.getItem('userRole') == '1') {
            this.rolesall = res.data.results;
        } else if (sessionStorage.getItem('userRole') == '2') {
            for (let v of res.data.results) {
                if (v.role_id != 1) {
                    this.rolesall.push(v);
                }
            }
        }
    }

    // get Service จุดบริการ
    async getServiceData() {
        const res: any = await this.usersService.listService();
        let service = [];
        service = res.data;
        if (service.ok) {
            this.services = service.results;
        } else {
            alert('Error loading users');
        }
    }

    // get Services data from API แสดงหน้าแรก
    async getData() {
        this.blockedPanel = true;
        try {
            let id: any = sessionStorage.getItem('hospitalId');
            let res: any;
            if (id == 1) {
                res = await this.usersService.list();
            } else {
                res = await this.usersService.getByHospitalId(id);
            }
            let users_ = [];
            users_ = await res.data;

            if (users_.results.length > 0) {
                this.users = await users_.results;
                for (let u of this.users) {
                    let profile: any = await this.usersService.getProfilesById(
                        u.user_id
                    );
                    // console.log("profile.data :",profile.data);

                    if (profile.data.results.length > 0) {
                        u.fullname = await profile.data.results[0].fullname;
                        u.position = await profile.data.results[0].position;
                        if (profile.data.results.length > 0) {
                            let hospital: any = await this.hospitals.find(
                                (h: any) =>
                                    h.hospital_id ==
                                    profile.data.results[0].hospital_id
                            );
                            u.hospital_name = await hospital.hospital_name;
                        }
                    }
                }
            } else {
                alert('Error loading users');
            }
            this.blockedPanel = false;
        } catch (error) {
            this.blockedPanel = false;

            console.log(error);
        }
    }

    // display form add data
    displayFormAdd() {
        this.displayClear();
        this.isAdd = true;
        this.isEdit = false;
        this.displayForm = true;
        this.userName = '';
        this.passWord = '';
        this.fullname = null;
        this.position = null;
        this.phone_number = null;
        this.title = null;
        this.cid = null;
        this.serviceId = null;
        this.hospitalId = null;
        this.selectedDrop = null;
    }
    displayClear() {
        this.userName = '';
        this.passWord = '';
        this.fullname = null;
        this.position = null;
        this.phone_number = null;
        this.title = null;
        this.cid = null;
        this.serviceId = null;
        this.hospitalId = null;
        this.selectedDrop = null;
    }

    // display form edit data
    async displayFormEdit(data: any) {
        this.displayClear();
        console.log('data', data);
        console.log('rolesall', this.rolesall);

        await this.getRolesData(data.role_id);
        await this.getProfilesData(data.user_id);

        this.isAdd = false;
        this.isEdit = true;
        // edit profile
        this.cidEdit = this.profiles.cid;
        this.fullNameEdit = this.profiles.fullname;
        this.hospitalId = this.profiles.hospital_id;
        this.serviceId = this.profiles.service_id;
        this.phoneNumberEdit = this.profiles.phone_number;
        this.posiTionEdit = this.profiles.position;
        this.tiTleEdit = this.profiles.title;
        this.userIdEdit = data.user_id;
        this.userNameEdit = data.username;
        this.passWordEdit = '';
        this.userActiveEdit = data.is_active;
        this.displayForm = true;

        for (let v of this.rolesall) {
            if (v.role_id == data.role_id) {
                this.selectedDrop = v;
                this.showDropdown(v.role_id);
            }
        }
    }

    async save() {
        this.saveUser();
    }

    async saveProfiles() {
        let service_id: any;

        if (this.is_service) {
            service_id = this.serviceId;
        } else {
            service_id = null;
        }

        let data = {
            cid: this.cid,
            fullname: this.fullname,
            phone_number: this.phone_number,
            position: this.position,
            title: this.title,
            user_id: this.user_id,
            hospital_id: this.hospitalId,
            service_id: service_id,
        };

        // save data profice
        let rs: any = await this.usersService.saveProfiles(data);
        console.log(rs);
    }

    // function save data
    async saveUser() {
        let isValidate = true;
        if (
            this.phone_number == '' ||
            this.phone_number == null ||
            this.phone_number == undefined ||
            this.phone_number.length != 10 ||
            !this.isNumber(this.phone_number)
        ) {
            isValidate = false;
            this.showMessages('error', 'Error', 'เบอร์โทรไม่ถูกต้อง');
            return;
        }
        if (
            this.passWord == '' ||
            this.passWord == null ||
            this.passWord == undefined ||
            this.passWord.length < 6
        ) {
            isValidate = false;
            this.showMessages('error', 'Error', 'รหัสผ่านน้อยกว่า 6 ตัวอักษร');
            return;
        }
        if (
            this.cid == '' ||
            this.cid == null ||
            this.cid == undefined ||
            this.cid.length != 13 ||
            !this.isNumber(this.cid)
        ) {
            isValidate = false;
            this.showMessages('error', 'Error', 'เลขบัตรประชาชนไม่ถูกต้อง');
            return;
        }
        console.log(isValidate);

        if (isValidate) {
            let data = {
                username: this.phone_number,
                password: this.passWord,
                role_id: this.selectedDrop.role_id,
            };
            try {
                // save data
                let rs: any = await this.usersService.save(data);

                if (rs.data.results.length > 0) {
                    this.user_id = rs.data.results[0].user_id;
                    this.saveProfiles();
                    // close form
                    this.displayForm = false;

                    //refresh data
                    await this.getHospitalData();
                    await this.getData();
                }
            } catch (error) {
                console.log(error);
            }
        }
    }

    async update() {
        this.updateUser();
    }

    showDropdownService() {
        this.is_service = true;
        console.log(this.is_service);
        console.log(this.serviceId);
    }

    // function update data profile
    async updateProfile() {
        let id = this.userIdEdit;

        let isValidate = true;
        if (
            this.phoneNumberEdit == '' ||
            this.phoneNumberEdit == null ||
            this.phoneNumberEdit == undefined ||
            this.phoneNumberEdit.length != 10 ||
            !this.isNumber(this.phoneNumberEdit)
        ) {
            isValidate = false;
            this.showMessages('error', 'Error', 'เบอร์โทรไม่ถูกต้อง');
            return;
        }

        let service_id: any;

        if (this.selectedDrop.role_id == 4) {
            this.hospitalId = 4;
        }

        if (this.is_service) {
            service_id = await this.serviceId;
        } else {
            service_id = null;
        }

        let body = {
            cid: this.cidEdit,
            fullname: this.fullNameEdit,
            hospital_id: this.hospitalId,
            phone_number: this.phoneNumberEdit,
            position: this.posiTionEdit,
            title: this.tiTleEdit,
            service_id: service_id,
        };
        // update data

        try {
            await this.usersService.updateProfiles(id, body);
        } catch (error) {
            console.log(error);
        }
    }

    async updateUser() {
        console.log(this.selectedDrop);

        let data: any;
        if (this.passWordEdit) {
            data = {
                username: this.userNameEdit,
                password: this.passWordEdit,
                is_active: this.userActiveEdit,
                role_id: this.selectedDrop.role_id,
            };
            // console.log(data);
        } else {
            data = {
                username: this.userNameEdit,
                is_active: this.userActiveEdit,
                role_id: this.selectedDrop.role_id,
            };
            // console.log(data);
        }

        // save data
        let rs: any = await this.usersService.update(this.userIdEdit, data);

        this.updateProfile();
        // close form
        this.displayForm = false;

        //refresh data
        await this.getHospitalData();
        await this.getData();
    }

    // set is active = false
    async disable(id: number) {
        let body = {
            is_active: false,
        };
        await this.usersService.update(id, body);

        // refresh data
        await this.getData();
    }

    // set is active = true
    async enable(id: number) {
        let body = {
            is_active: true,
        };
        await this.usersService.update(id, body);

        // refresh data
        await this.getData();
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image =
            this.layoutService.config.colorScheme === 'dark'
                ? 'line-effect-dark.svg'
                : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }

    async showDropdown(id: any) {
        let hospital: any = [];
        this.hospitals = [];

        if (id == 3) {
            this.is_service = true;
        } else if(id == 4) {
            this.is_hospital = false;
            this.is_service = false;
        } else {
            this.is_service = false;
            this.is_hospital = true;
        }
        if (id == 1) {
            for (let v of this.hospitalBack) {
                if (v.hospital_id == 1) {
                    hospital.push(v);
                }
            }
            this.hospitals = hospital;
        } else {
            for (let v of this.hospitalBack) {
                if (v.hospital_id == 4) {
                } else {
                    hospital.push(v);
                }
            }
            this.hospitals = hospital;
        }
    }

    // alert message
    showMessages(severity: any, summary: any, detail: any) {
        this.messages = [
            { severity: severity, summary: summary, detail: detail },
        ];
    }

    clearMessages() {
        this.messages = [];
    }

    isNumber(value?: string | number) {
        return (
            value != null && value !== '' && !isNaN(Number(value.toString()))
        );
    }
}
